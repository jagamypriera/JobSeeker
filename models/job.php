<?php
  class Job {
    public $jobs;

    public function __construct($jobs) {
      $this->jobs=$jobs;
    }

    public static function all() {
      $db = Db::getInstance();
      $req = $db->prepare('SELECT * FROM jobs');
      $req->execute();
      $query=$req->fetchAll(PDO::FETCH_ASSOC);
      return $query;
    }

    public static function get($id) {
      $db = Db::getInstance();
      $id = intval($id);
      $req = $db->prepare('SELECT * FROM jobs WHERE id = :id');
      $req->execute(array('id' => $id));
      $jobs = $req->fetch(PDO::FETCH_ASSOC);
      if($req->fetchColumn())
        return false;
      return $jobs;
    }
    public static function search($data){
      $db = Db::getInstance();
      $req=$db->prepare("SELECT * FROM jobs WHERE (title LIKE :keyword 
        OR description LIKE :keyword 
        OR company LIKE :keyword) 
      AND location LIKE :location");
      $req->bindValue(':keyword','%'.$data['keyword'].'%');
      $req->bindValue(':location','%'.$data['location'].'%');
      $req->execute();
      $jobs = $req->fetchAll(PDO::FETCH_ASSOC);
      if($req->rowCount()>0)
        return $jobs;
      return false;
    }
    public static function create($data){
      $db = Db::getInstance();
      session_start();
      $loggedUser=$_SESSION["id"];
      $req = $db->prepare("INSERT INTO jobs(
            title,
            brief,
            description,
            requirements,
            created,
            location,
            type,
            company,
            photo,
            sallary,
            category,
            published_by) VALUES (
            :title, 
            :brief, 
            :description,
            :requirements,
            :created,
            :location,
            :type,
            :company,
            :photo,
            :sallary,
            :category,
            :published_by)");
      $status= $req->execute(array(
        'title' => $data['title'],
        'brief' => $data['brief'],
        'description' => $data['description'], 
        'requirements' => $data['requirements'], 
        'created' => date("Y-m-d H:i:s"),
        'location' => $data['location'], 
        'type' => $data['type'], 
        'company' => $data['company'], 
        'photo' => $data['photo'], 
        'sallary' => $data['sallary'], 
        'category' => $data['category'], 
        'published_by' => $loggedUser
        )
      );
      if($status)
        return true;
      return false;
    }
     public static function update($data){
      $db = Db::getInstance();
      session_start();
      $loggedUser=$_SESSION["id"];
      $req = $db->prepare("UPDATE jobs SET
            title=:title,
            brief=:brief,
            description=:description,
            requirements=:requirements,
            created=:created,
            location=:location,
            type=:type,
            company=:company,
            photo=:photo,
            sallary=:sallary,
            category=:category,
            published_by=:published_by
            WHERE id = :id");
      $status=$req->execute(array(
        'title' => $data['title'],
        'brief' => $data['brief'],
        'description' => $data['description'], 
        'requirements' => $data['requirements'], 
        'created' => date("Y-m-d H:i:s"),
        'location' => $data['location'], 
        'type' => $data['type'], 
        'company' => $data['company'], 
        'photo' => $data['photo'], 
        'sallary' => $data['sallary'], 
        'category' => $data['category'], 
        'published_by' => $loggedUser,
        'id' => $data['id']
        )
      );
      if($status)
        return true;
      return false;
    }
    public static function delete($data){
      $db = Db::getInstance();
      $sql = "DELETE FROM jobs WHERE id =  :id";
      $stmt = $db->prepare($sql);
      $stmt->bindParam(':id', $data['id'], PDO::PARAM_INT);   
      $status=$stmt->execute();
      if($stmt->rowCount()>0)
        return true;
      return false;
    }
  }
?>