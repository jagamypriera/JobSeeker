<?php
  require_once('models/user.php');
  class UsersController {
    public function create($param) {
      $data = array();
      parse_str($param, $data);
      if(!User::exists($data['email'])){
        User::create($data);
        $result = array('result' => 'success', 'data'=>'Akun telah dibuat, silahkan masuk untuk melanjutkan');
        echo json_encode($result);
      }else{
        $result = array('result' => 'failed', 'message'=>'Email telah terdaftar');
        echo json_encode($result);
      }
    }
    public function login($param) {
      $data = array();
      parse_str($param, $data);
      $query=User::login($data);
      if($query==false){
        $result = array('result' => 'failed', 'message'=>'Password atau email salah');
        echo json_encode($result);
      }else{
        session_start();
        $_SESSION['id'] = $query['id'];
        $result = array('result' => 'success', 'data'=>$query);
        echo json_encode($result);
      }
    }
    public function logout() {
      session_start();
      if(session_destroy()){
        $result = array('result' => 'success', 'data'=>'Berhasil Keluar');
        echo json_encode($result);
      }
    }
    public static function isLogin() {
      session_start();
      if(!isset($_SESSION["id"]))
        echo "false";
      else
      echo "true";
    }
  }
?>