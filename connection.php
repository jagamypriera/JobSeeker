<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
  class Db {
    private static $instance = NULL;

    private function __construct() {}

    private function __clone() {}

    public static function getInstance() {
      if (!isset(self::$instance)) {
        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        self::$instance = new PDO('mysql:host=localhost;dbname=jobber', 'root', '', $pdo_options);
      }
      return self::$instance;
    }
  }
?>